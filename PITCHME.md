# Geo-tecnologías y sensado remoto para aplicaciones en Salud Pública

<br>
Dra. Verónica Andreo<br><br>
@img[width=500px](assets/img/banner_blanco.png)
<br>

---

@snap[west text-left]
## Contenidos
<br>
- Teledetección
- Sistemas de Información Geográfica (SIG)
- Ecología de las enfermedades
- Aplicaciones de la teledeteccion y los SIG en Ecología de las enfermedades
- Objetivos del Desarrollo Sustentable (ODS)
- Desafíos a la hora de usar teledetección para ODS
@snapend

---?image=assets/img/satellite_and_earth.jpg&size=cover

@transition[none]

@snap[south-east]
# **Teledetección**
@snapend

+++

## Qué es la teledetección?

<br>

> Adquisición de datos de la superficie terrestre desde sensores 
> instalados en plataformas espaciales, en virtud de la interacción
> electromagnética existente entre la tierra y el sensor.

<br>

@img[EO](https://www.asc-csa.gc.ca/images/satellites/quotidien/satellites-dans-notre-quotidien-ban.jpg)

Note: 

fuente de radiación el sol (teledetección pasiva),
fuente de radiacion el propio sensor (teledetección activa)

+++

@snap[west span-40]
## Diferentes tipos de resolución
<br>
- Espacial
- Temporal
- Espectral
- Radiométrica
@snapend

@snap[east span-65]
<br>
@img[](assets/img/all_resolutions_relation.png)
@snapend

+++

## Ventajas de la teledetección

<br>
- Visión global
- Observación a distintas escalas
- Cobertura frecuente y sistemática
- Homogeneidad en la adquisición
- Regiones visibles y no visibles del espectro
- Formato digital: fácil ingestión, procesamiento y análisis
- Gran disponibilidad de datos libres

---?image=assets/img/Paracatu-River_Brazil.jpg&size=cover

@snap[south-east]
# **Sistemas de Información Geográfica**
@snapend

+++

@snap[west span-60]
Un SIG descompone la realidad en temas, i.e., en distintas capas o 
estratos de información de la zona que se desea estudiar
<br><br>
**Gran ventaja:**
<br>
Permite relacionar y combinar las distintas capas entre sí, para 
contestar preguntas complejas y obtener nueva información.
@snapend

@snap[east span-40]
@img[](assets/img/GIS_layers.jpg)
@snapend

Note:

Un sistema de hardware, software y procedimientos diseñado para realizar la captura, almacenamiento, manipulación, análisis, modelización y presentación de datos referenciados espacialmente para la resolución de problemas complejos de planificación y gestión

+++

## Preguntas que se pueden responder con herramientas SIG

<br>
- *Ubicación*: ¿Dónde se encuentra... ?
- *Tendencias*: ¿Qué ha cambiado desde... ?
- *Patrones*: ¿Qué patrones espaciales existen... ?
- *Proximidad*: ¿Cómo es el área alrededor de... ?
- *Redes*: ¿Cuál es la mejor ruta entre A y B?
- *Operaciones lógicas*: ¿Dónde ocurre la combinación de las condiciones C y D?
- *Temporal*: ¿Cuándo ocurren ciertas condiciones?
- *Modelos/Simulaciones*: ¿Qué pasaría si... ?

---

## Ecología de las enfermedades

<br>
El principal objetivo de la *ecología de las enfermedades* es comprender la influencia 
de factores ambientales y predecir cuándo y dónde es más probable que ocurra una enfermedad

<br>@fa[angle-double-down fa-3x]<br>

*toma de decisiones, planificación de acciones de prevención, manejo y/o respuesta, etc.*

+++

Qué podemos **ver** con la teledetección para aportar a la ecología de enfermedades?
 
<br>
@img[width=550px](assets/img/lambin_rs_env_soc.png)

@size[16px](*Lambin et al., 2010.*)

+++

Y cómo usamos la información derivada de la teledetección? 

<br>
@img[](assets/img/workflow_sdm.png)

---

## Aplicaciones de la teledetección y los SIG en ecología de enfermedades

<br>
- Espacialización de brotes (patrones y causas)
- Mapeo de distribución de huéspedes y vectores
- Estimación de índices ambientales y monitoreo
- Mapeo del riesgo de enfermedad y predicción de número de casos
- Análisis de paisaje: métricas, fragmentación, LULCC
- Emplazamiento de servicios de salud 
- Localización de las mejores rutas a hospitales

+++

@snap[north span-100]
## Distribución de roedores y riesgo de Hantavirus
@snapend

@snap[west span-50]
<br><br>
@img[](assets/img/sph_workflow.png)
@snapend

@snap[east span-50]
<br><br>
@img[width=500px](assets/img/HPS_south_arg.png)
@snapend

@snap[south span-100 text-05]
<a href="https://doi.org/10.1007/s10393-011-0719-5">*Andreo et al., 2011*</a> & <a href="https://doi.org/10.3390/v6010201">*Andreo et al., 2014*</a>
@snapend

+++

@snap[north span-100]
## WNV y anomalías en LST
@snapend

@snap[west span-50 text-09]
<br>
Workflow de reconstrucción de datos LST de MODIS
@img[width=400px](https://www.mdpi.com/remotesensing/remotesensing-09-01333/article_deploy/html/images/remotesensing-09-01333-g001-550.jpg)
@snapend

@snap[east span-50 text-09]
<br>
Resultado de Co-clustering para índices derivados de LST
@img[](assets/img/wnv_cocluster.png)
@snapend

@snap[south span-100 text-05]
<a href="https://doi.org/10.3390/rs9121333">*Metz et al., 2017*</a> & <a href="https://doi.org/10.1109/IGARSS.2018.8519542">*Andreo et al., 2018*</a>
@snapend

+++

@snap[north span-100]
## Leishmaniasis y cambios en el uso/cobertura del suelo
@snapend

@snap[west span-50]
<br><br>
Mapa de cambios
@img[](assets/img/change_maps.png)
@snapend

@snap[east span-50]
<br><br>
Probabilidad de LC
@img[](assets/img/fig_ensemble_final.png)
@snapend

+++

@snap[north span-100]
## Dengue: patrones temporales y micro-hábitat
@snapend

@snap[west span-45 text-left]
- Patrones temporales y espaciales en *Aedes aegypty* en Córdoba
- Asociación con variables derivadas del análisis de imágenes SPOT (6m)
@snapend

@snap[east span-55]
<br><br><br>
@img[time series](assets/img/pam_series.png)
<br>
@img[temp pattern in space](assets/img/clustering_maps.png)
@snapend

@snap[south span-100 text-05]
<a href="https://ieeexplore.ieee.org/document/8882184">*Andreo et al., 2019*</a>
@snapend

+++

## Dengue: hacia un workflow operativo 

<br>
https://geoportal.conae.gov.ar/geoexplorer/composer/

@img[width=800px](assets/img/dengue_conae_example.png)

@size[16px](<a href="https://doi.org/10.4081/gh.2012.120">*Porcasi et al., 2012*</a>)

+++

@snap[north span-100]
## Dengue: hacia un workflow operativo 
@snapend

@snap[west span-65]
<br>
@img[](assets/img/workflow_dengue.png)
@snapend

@snap[east span-35]
<br>
@img[](assets/img/Predictions.png)
@snapend

+++

## Malaria en alta resolución

<br>
@img[width=410px](https://camo.githubusercontent.com/a93e6f821187dda1eaaec9c39741382dded8a40d/687474703a2f2f7777772e6d6470692e636f6d2f72656d6f746573656e73696e672f72656d6f746573656e73696e672d30392d30303335382f61727469636c655f6465706c6f792f68746d6c2f696d616765732f72656d6f746573656e73696e672d30392d30303335382d61672e706e67)
@img[width=455px](assets/img/malaria_pr.png)

@size[16px](<a href="https://doi.org/10.3390/rs9040358">*Grippa et al., 2017*</a> & <a href="https://doi.org/10.1186/s12942-020-00232-2">*Georganos et al., 2020*</a>)

---

@img[](assets/img/sdg.png)

+++

@img[](assets/img/sdg3.3_indicators.png)

https://sdgs.un.org/goals/goal3

+++

@img[](assets/img/sdg3.d_indicator.png)

https://sdgs.un.org/goals/goal3

---?image=assets/img/landsatlooks.jpg&opacity=70&position=left&size=45% 100%

@snap[north-east span-50]
## Geo-tecnologías, teledetección y ODS 3
@snapend

@snap[east span-55 text-center]
<br><br>
> *La principal contribución está en la generación de líneas de base y, el diseño e implementación de sistemas operativos de mapeo de distribución y riesgo, monitoreo y alerta temprana a diferentes escalas*
@snapend

+++

## Dificultades y desafíos I

<br>
- Compromiso entre diferentes resoluciones 
- VHR, LiDAR, UAV, Hyper-spectral (U$S)
- Datos faltantes, nubes, sombras (interpolaciones)
- Necesidad de correcciones de diversos tipos
- Grandes volúmenes de datos vs ancho de banda y capacidad de almacenamiento y cómputo (cloud computing, paralelización, tiempo de aprendizaje y U$S)

<br>
> *Nunca va a reemplazar los datos de campo!* 

+++

## Dificultades y desafíos II

<br>
- Compromiso de las autoridades y asignación de recursos
- Actualización y digitalización de datos de casos e intervenciones
- Armonización de los registros a diferentes niveles
- Acceso público a los registros
- Capacitación del personal
- Desarrollo de métodos estandarizados, reproducibles y escalables

+++

## Hay mucho lugar para seguir haciendo, mejorando y aprendiendo!!
<br>
Están todos invitados!

---?image=assets/img/CONAE_aereo.png&opacity=70&position=left&size=52% 100%

@snap[south-west span-50 text-07 text-left]
Los esperamos!!
[Instituto Gulich](https://ig.conae.unc.edu.ar/)
[@fab[youtube]](https://www.youtube.com/channel/UCI-yqSH5XPVwnBM5mOyOCHg) [@fab[instagram]](https://www.instagram.com/institutogulich/) [@fab[facebook]](https://www.facebook.com/institutogulich.unc.conae/) [@fab[linkedin]](https://www.linkedin.com/in/instituto-mario-gulich-746a2418b/v)
@snapend

@snap[north-east span-50]
## Muchas gracias por su atención!
@snapend

@snap[south-east text-right text-06]
@img[width=90px](assets/img/vero_round_small.png)
[@fa[globe]veroandreo.gitlab.io](https://veroandreo.gitlab.io) 
[@fa[envelope]veroandreo@gmail.com](veroandreo@gmail.com)
@snapend

---

@snap[south span-50]
@size[18px](Presentation powered by)
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="30%"></a>
@snapend