<a href="https://cods.uniandes.edu.co/imagenes-satelitales-para-predecir-enfermedades-transmitidas-por-vectores/"> 
<img src="assets/img/featured.jpeg" width="750%" align="center"></a>

# Geo-tecnologías y sensado remoto para aplicaciones en Salud Pública

Una sexta parte de las enfermedades y discapacidades sufridas en todo 
el mundo se deben a enfermedades transmitidas por vectores, y más de 
la mitad de la población mundial se encuentra actualmente en situación 
de riesgo según la Organización Mundial de la Salud. Frente a este 
escenario, y los cambios producto de la crisis climática global, las 
geo-tecnologías y el sensado remoto, han adquirido una renovada 
relevancia en lo referente a aplicaciones en Salud Pública. Los datos
provenientes de sensado remoto tienen claras ventajas sobre las 
mediciones terrestres convencionales porque pueden recoger información
global, de forma repetida y automática. Esto permite contar con 
información sobre factores ambientales estrechamente relacionados con
la ocurrencia de vectores y reservorios de enfermedades. Estos factores
ambientales se utilizan como co-variables en modelos de distribución 
potencial de vectores y reservorios, como así también en la predicción 
de la probabilidad de ocurrencia de enfermedades. Los mapas basados en 
predicciones espaciales de dichos modelos pueden utilizarse para orientar 
geográficamente intervenciones como tratamiento con drogas, prevención, 
control vectorial o ambiental, asignación de recursos, etc. En esta 
charla haré un recorrido por distintas aplicaciones del sensado remoto 
y las geo-tecnologías a enfermedades como hantavirus, dengue, fiebre del 
Nilo Occidental y leishmaniasis. Se examinarán también distintas 
limitaciones, desafíos y direcciones futuras vinculadas al desarrollo de 
herramientas para la medición de los objetivos del desarrollo sustentable.

---
Haga click sobre la imagen para ir a la presentación: 

<a href="https://gitlab.com/veroandreo/presentacion-codspace/-/blob/master/pdf/codspace.pdf"> 
<img src="assets/img/portada.png" width="750%" align="center"></a>

## Referencias

- **Andreo, V.**, Glass, G., Shields, T., Provensal, C., & Polop, J. (2011). Modeling Potential Distribution of *Oligoryzomys longicaudatus*, the Andes Virus (Genus: Hantavirus) Reservoir, in Argentina. EcoHealth, 8(3), 332–348. https://doi.org/10.1007/s10393-011-0719-5
- Porcasi, X., Rotela, C. H., Introini, M. V., Frutos, N., Lanfri, S., Peralta, G., Elia, E. A. D., Lanfri, M. A., & Scavuzzo, C. M. (2012). An operative dengue risk stratification system in Argentina based on geospatial technology. 6(3). https://doi.org/10.4081/gh.2012.120
- **Andreo, V.**, Neteler, M., Rocchini, D., Provensal, C., Levis, S., Porcasi, X., Rizzoli, A., Lanfri, M., Scavuzzo, M., Pini, N., Enria, D., & Polop, J. (2014). Estimating Hantavirus Risk in Southern Argentina: A GIS-Based Approach Combining Human Cases and Host Distribution. Viruses, 6(1), 201–222. https://doi.org/10.3390/v6010201
- Metz, M., **Andreo, V.**, & Neteler, M. (2017). A New Fully Gap-Free Time Series of Land Surface Temperature from MODIS LST Data. Remote Sensing, 9(12), 1333. https://doi.org/10.3390/rs9121333
- **Andreo, V.**, Izquierdo-Verdiguier, E., Zurita-Milla, R., Rosa, R., Rizzoli, A., & Papa, A. (2018). Identifying Favorable Spatio-Temporal Conditions for West Nile Virus Outbreaks by Co-Clustering of MODIS LST Indices Time Series. IGARSS 2018 - 2018 IEEE International Geoscience and Remote Sensing Symposium, 4670–4673. https://doi.org/10.1109/IGARSS.2018.8519542
- **Andreo, V.**, Porcasi, X., Rodriguez, C., Lopez, L., Guzman, C., & Scavuzzo, C. M. (2019). Time Series Clustering Applied to Eco-Epidemiology: The case of *Aedes aegypti* in Córdoba, Argentina. 2019 XVIII Workshop on Information Processing and Control (RPIC), 93–98. https://doi.org/10.1109/RPIC.2019.8882184
- Georganos, S., Brousse, O., Dujardin, S., Linard, C., Casey, D., Milliones, M., Parmentier, B., van Lipzig, N. P. M., Demuzere, M., Grippa, T., Vanhuysse, S., Mboga, N., **Andreo, V.**, Snow, R. W., & Lennert, M. (2020). Modelling and mapping the intra-urban spatial distribution of *Plasmodium falciparum* parasite rate using very-high-resolution satellite derived indicators. International Journal of Health Geographics, 19(1), 38. https://doi.org/10.1186/s12942-020-00232-2
